import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class HotelBooking {

    public static void main(String[] args){

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\najib.zainudin\\Documents\\Adaptavist_task\\phptravel\\Tools\\chromedriver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        String URL = "https://www.phptravels.net/login";
        String expectedTitle = "Login";
        String actualTitle;

        //Launch login page
        driver.get(URL);

        driver.manage().window().maximize();

        //Login with valid credentials
        driver.findElement(By.name("username")).sendKeys("testhappy@gmail.com");
        driver.findElement(By.name("password")).sendKeys("asdf1234");

        driver.findElement(By.xpath("//button[contains(@class, 'btn btn-primary btn-lg btn-block loginbtn')]")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Click Home tab
        driver.findElement(By.xpath("//a[contains(@title, 'home')]")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Set hotel location
        driver.findElement(By.xpath("//input[contains(@type, 'text')]")).sendKeys("Singapore");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//*[contains(text(), 'Rendezvous Hotel ')]")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Set check in date
        WebElement checkinDate = driver.findElement(By.id("checkin"));
        checkinDate.clear();
        checkinDate.sendKeys("11/11/2020");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Set checkout date
        WebElement checkoutDate = driver.findElement(By.id("checkout"));
        checkoutDate.clear();
        checkoutDate.sendKeys("15/11/2020");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Search for available hotel
        driver.findElement(By.xpath("//*[contains(text(), '\n" +
                "             Search            ')]")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Select hotel room
        driver.findElement(By.xpath("//*[contains(text(), '\n" +
                "          See price and date          ')]")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.findElement(By.cssSelector("label[for=\"400\"]")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//*[contains(text(), '\n" +
                "                Book Now                ')]")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Set personal details
        driver.findElement(By.name("firstname")).sendKeys("Test");
        driver.findElement(By.name("lastname")).sendKeys("Happy");
        driver.findElement(By.name("email")).sendKeys("testhappy0@gmail@com");
        driver.findElement(By.name("confirmemail")).sendKeys("testhappy0@gmail@com");
        driver.findElement(By.xpath("//input[contains(@name, 'phone')]")).sendKeys("+00000000000");

        driver.findElement(By.cssSelector("a[class='chosen-single']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        Select selectByVisibleText = new Select (driver.findElement(By.cssSelector("ul[class='chosen-result']")));
        selectByVisibleText.selectByVisibleText("Albania");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.findElement(By.cssSelector("input[class=\"o2 coupon form-bg-light\"]")).sendKeys("adaptavist");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//*[contains(text(), 'CONFIRM THIS BOOKING')]")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Click Pay Now
        driver.findElement(By.cssSelector("button[class=\"btn btn-primary\"]")).click();

        //Set payment method
        driver.findElement(By.cssSelector("div[class=\"chosen-container chosen-container-single chosen-container-single-nosearch chosen-with-drop chosen-container-active\"]")).click();
        driver.findElement(By.xpath("//*[contains(text(), 'Credit Card')]")).click();

        //Set banking details
        driver.findElement(By.name("firstname")).sendKeys("Test");
        driver.findElement(By.name("lastname")).sendKeys("Happy");
        driver.findElement(By.name("cardnum")).sendKeys("0000000000");
        driver.findElement(By.name("cvv")).sendKeys("123");
        driver.findElement(By.cssSelector("button[class=\"btn btn-success btn-lg paynowbtn go-right\"]")).click();

        actualTitle = driver.getTitle();

        if (actualTitle.contentEquals(expectedTitle)){
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }

        driver.quit();

    }
}
