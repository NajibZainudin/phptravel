import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import java.util.concurrent.TimeUnit;

public class RegisterAccount {

    public static void main(String[] args){

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\najib.zainudin\\Documents\\Adaptavist_task\\phptravel\\Tools\\chromedriver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        String URL = "https://www.phptravels.net/register";
        String expectedTitle = "Login";
        String actualTitle;

        //Launch page to register account
        driver.get(URL);

        driver.manage().window().maximize();

        //Insert account details
        driver.findElement(By.name("firstname")).sendKeys("Test");
        driver.findElement(By.name("lastname")).sendKeys("Happy");
        driver.findElement(By.xpath("//input[contains(@name, 'phone')]")).sendKeys("+00000000000");
        driver.findElement(By.xpath("//input[contains(@name, 'email')]")).sendKeys("testhappy@gmail.com");
        driver.findElement(By.xpath("//input[contains(@name, 'password')]")).sendKeys("asdf1234");
        driver.findElement(By.xpath("//input[contains(@name, 'confirmpassword')]")).sendKeys("asdf1234");

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Click on Sign up
        driver.findElement(By.xpath("//button[contains(@class, 'signupbtn btn_full btn btn-success btn-block btn-lg')]")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Click on Account
        WebElement profile_button=driver.findElement(By.xpath("//li[@class='d-none d-md-block fl']/descendant::a[@class='btn btn-text-inherit btn-interactive']"));
        profile_button.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Logout account
        WebElement logout_button=driver.findElement(By.xpath("//li[@class='d-none d-md-block fl']/descendant::a[@class='dropdown-item tr']"));
        logout_button.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        actualTitle = driver.getTitle();

        if (actualTitle.contentEquals(expectedTitle)){
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }

        driver.quit();

    }
}
