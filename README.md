Task 2: phptravel 

Requirements:
 
| Software | Minimum version |  
| --- | --- |  
| IntelliJ Idea | 2019.1 or later|
| Oracle Java SDK or equivalent | 11.0 or later |

---

## Test Case Location

Test cases should be located in the following path:

\phptravel\TestCases\src

---

## How to run project

Before running any test case, please refer to the following steps:

1. checkout master branch
2. Run RegisterAccount.java
3. Run HotelBooking.java
